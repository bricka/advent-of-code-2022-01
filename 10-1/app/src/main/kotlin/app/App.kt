package app

import java.io.File

val INTERESTING_CYCLES: Set<Int> = setOf(20, 60, 100, 140, 180, 220)

sealed class Command () {
    abstract fun tick(): Command?
}

object NoopCommand: Command() {
    override fun tick() = null
}

data class AddxCommand private constructor (public val amount: Int, private val cycles: Int): Command() {
    constructor(amount: Int) : this(amount, 2)

    override fun tick() = if (cycles == 1) {
        null
    } else {
        AddxCommand(amount, cycles-1)
    }
}

fun parseLine(line: String): Command {
    val parts = line.split(" ")
    return when(parts[0]) {
        "noop" -> NoopCommand
        "addx" -> AddxCommand(parts[1].toInt())
        else -> throw Exception("Unknown command: ${parts[0]}")
    }
}

tailrec fun processCommands(signalSumSoFar: Int, cycle: Int, value: Int, queuedCommand: Command?, lines: List<String>): Int {
    println("processCommands($signalSumSoFar, $cycle, $value, $queuedCommand, $lines)")

    if (queuedCommand == null && lines.isEmpty()) {
        return signalSumSoFar
    }

    val newSum = if (INTERESTING_CYCLES.contains(cycle)) {
        signalSumSoFar + (cycle * value)
    } else {
        signalSumSoFar
    }

    val commandToQueue = if (queuedCommand == null) {
        parseLine(lines.first())
    } else {
        queuedCommand
    }.tick()

    val remainingLines = if (queuedCommand == null) {
        lines.subList(1, lines.size)
    } else {
        lines
    }

    val newValue = if (queuedCommand is AddxCommand && commandToQueue == null) {
        value + queuedCommand.amount
    } else {
        value
    }

    return processCommands(newSum, cycle + 1, newValue, commandToQueue, remainingLines)
}

fun main(args: Array<String>) {
    val lines = File(args.first()).readLines()
    processCommands(0, 1, 1, null, lines)
}
