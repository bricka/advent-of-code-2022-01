package app

import java.io.File
import kotlin.math.abs
import com.github.recognized.kotlin.ranges.union.LongRangeUnion

val MAX = 4000000L

val SENSOR_REGEX = """Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)""".toRegex()

data class Point(val x: Long, val y: Long) {
    fun distanceToPoint(other: Point): Long =
        abs(x - other.x) + abs(y - other.y)
}

data class Sensor(val location: Point, val nearestBeacon: Point) {
    private val distance = location.distanceToPoint(nearestBeacon)

    fun containsPoint(point: Point): Boolean =
        location.distanceToPoint(point) <= location.distanceToPoint(nearestBeacon)

    fun xRangeInRow(row: Long): LongRange? {
        val leftover = distance - abs(location.y - row)
        if (leftover < 0) {
            return null
        }
        val min = location.x - leftover
        val max = location.x + leftover
        return LongRange(Math.max(0, min), Math.min(max, MAX))
    }
}

fun parseLines(lines: List<String>): List<Sensor> =
    lines.map { line ->
        val (locX, locY, beaconX, beaconY) = SENSOR_REGEX.matchEntire(line)!!.destructured
        Sensor(Point(locX.toLong(), locY.toLong()), Point(beaconX.toLong(), beaconY.toLong()))
    }

fun main(args: Array<String>) {
    val sensors = parseLines(File(args.first()).readLines())
    val beaconXsByY = sensors.map { it.nearestBeacon }.groupBy { it.y }.mapValues { entry -> entry.value.map { it.x } }

    for (y in 0..MAX) {
        // println("y = $y")
        val rangeUnion = LongRangeUnion()
        val range = sensors.map { it.xRangeInRow(y) }.filter {it != null }
        range.forEach { rangeUnion.union(it!!) }
        beaconXsByY.getOrDefault(y, listOf()).forEach { rangeUnion.union(LongRange(it, it)) }

        // println(rangeUnion)

        if (rangeUnion.contains(LongRange(0, MAX))) {
            continue
        }

        for (x in 0..MAX) {
            if (!rangeUnion.contains(LongRange(x, x))) {
                println("$x, $y")
                val tuningFrequency = x * 4000000 + y
                println(tuningFrequency)
                return
            }
        }

        throw Error("Foo")
    }
}
