package app

import kotlin.test.Test
import kotlin.test.assertEquals

class IntMultiRangeTest {
    @Test
    fun `works with a single range`() {
        val range = 1..3
        val multiRange = IntMultiRange(listOf(range))
        assertEquals(true, multiRange.contains(2))
        assertEquals(false, multiRange.contains(4))
    }

    @Test
    fun `works with two ranges`() {
        val multiRange = IntMultiRange(listOf(1..3, 6..8))
        assertEquals(true, multiRange.contains(2))
        assertEquals(true, multiRange.contains(6))
        assertEquals(true, multiRange.contains(7))

        assertEquals(false, multiRange.contains(0))
        assertEquals(false, multiRange.contains(4))
        assertEquals(false, multiRange.contains(9))
    }

    @Test
    fun `can union new ranges`() {
        val mr1 = IntMultiRange(listOf(1..3))
        assertEquals(true, mr1.contains(2))
        assertEquals(false, mr1.contains(7))

        val mr2 = mr1.union(6..8)
        assertEquals(true, mr2.contains(2))
        assertEquals(true, mr2.contains(7))
    }

    @Test
    fun `can union new ranges in the middle`() {
        val mr1 = IntMultiRange(listOf(1..3))
        assertEquals(true, mr1.contains(2))
        assertEquals(false, mr1.contains(7))
        assertEquals(false, mr1.contains(11))

        val mr2 = mr1.union(10..20)
        assertEquals(true, mr2.contains(2))
        assertEquals(false, mr2.contains(7))
        assertEquals(true, mr2.contains(11))

        val mr3 = mr2.union(6..8)
        assertEquals(true, mr3.contains(2))
        assertEquals(true, mr3.contains(7))
        assertEquals(true, mr3.contains(11))
    }

    @Test
    fun `can union new ranges with overlap`() {
        val mr1 = IntMultiRange(listOf(1..5))
        assertEquals(true, mr1.contains(2))
        assertEquals(true, mr1.contains(4))
        assertEquals(false, mr1.contains(9))

        val mr2 = mr1.union(4..10)
        assertEquals(true, mr2.contains(2))
        assertEquals(true, mr2.contains(4))
        assertEquals(true, mr2.contains(9))
    }
}
