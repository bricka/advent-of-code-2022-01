package app

import java.io.File
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.jsonArray
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.jsonPrimitive
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.intOrNull
import kotlinx.serialization.json.int

fun JsonElement.asList(): List<JsonElement> = when(this) {
    is JsonArray -> this
    is JsonPrimitive -> listOf(this)
    else -> throw Exception("Unknown type: $this")
}

object ArrayComparator : Comparator<List<JsonElement>> {
    override fun compare(a1: List<JsonElement>, a2: List<JsonElement>): Int {
        println("Comparing $a1 and $a2")
        for (i in (0..a1.size)) {
            val e1 = a1.getOrNull(i)
            val e2 = a2.getOrNull(i)
            println("$e1 -- $e2")

            if (e1 == null && e2 == null) {
                return 0
            }
            if (e1 == null) {
                return -1
            }
            if (e2 == null) {
                return 1
            }
            if (e1 is JsonArray || e2 is JsonArray) {
                val comp = ArrayComparator.compare(e1.asList(), e2.asList())
                if (comp != 0) {
                    return comp
                } else {
                    continue
                }
            }

            val comp = e1.jsonPrimitive.int - e2.jsonPrimitive.int
            if (comp != 0) {
                return comp
            } else {
                continue
            }
        }

        return 0
    }
}

fun main(args: Array<String>) {
    val dividerPacket2 = JsonArray(JsonArray(listOf(JsonPrimitive(2))))
    val dividerPacket6 = JsonArray(JsonArray(listOf(JsonPrimitive(6))))

    val packets: List<JsonArray> = File(args.first()).readLines().filter { it != "" }.map { Json.parseToJsonElement(it).jsonArray } + listOf(dividerPacket2, dividerPacket6)
    val decoderKey = packets.sortedWith(ArrayComparator).mapIndexed { i, p -> when(p) {
        dividerPacket2, dividerPacket6 -> i + 1
        else -> 1
    }}.reduce{ prev, curr -> prev * curr }
    println(decoderKey)

}
