package day2

import java.io.File

enum class Result(val points: Int) {
    Beats(6),
    Loses(0),
    Tie(3)
}

sealed class Shape(val points: Int) {
    abstract val beats: Shape
    abstract val losesTo: Shape

    fun result(other: Shape): Result {
        if (this == other) {
            return Result.Tie
        } else if (this.beats == other) {
            return Result.Beats
        } else {
            return Result.Loses
        }
    }

    fun toAchieveResult(result: Result): Shape {
        return when(result) {
            Result.Tie -> this
            Result.Loses -> this.beats
            Result.Beats -> this.losesTo
        }
    }

    object Rock: Shape(1) {
        override val beats = Scissors
        override val losesTo = Paper
    }

    object Paper: Shape(2) {
        override val beats = Rock
        override val losesTo = Scissors
    }

    object Scissors: Shape(3) {
        override val beats = Paper
        override val losesTo = Rock
    }
}

data class Round(val opponent: Shape, val result: Result) {
    fun getPoints(): Int {
        val whatToDo = opponent.toAchieveResult(result)

        return result.points + whatToDo.points
    }
}

fun shapeForOpponent(code: String): Shape {
    return when(code) {
        "A" -> Shape.Rock
        "B" -> Shape.Paper
        "C" -> Shape.Scissors
        else -> throw Exception("Invalid opponent shape: $code")
    }
}

fun resultFromCode(code: String): Result {
    return when(code) {
        "X" -> Result.Loses
        "Y" -> Result.Tie
        "Z" -> Result.Beats
        else -> throw Exception("Invalid me shape: $code")
    }
}

fun roundFromLine(line: String): Round {
    val (opponent, result) = line.split(" ")
    return Round(shapeForOpponent(opponent), resultFromCode(result))
}

fun main(args: Array<String>) {
    val inputFilename = args.firstOrNull()
    if (inputFilename == null) {
        throw Exception("Must provide an input filename")
    }

    val points = File(inputFilename).useLines { lines ->
        lines.fold(0) { prev, curr -> prev + roundFromLine(curr).getPoints() }
    }

    println(points)
}
