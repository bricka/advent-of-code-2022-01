package app

import java.io.File
import kotlinx.collections.immutable.PersistentSet
import kotlinx.collections.immutable.persistentSetOf
import kotlinx.collections.immutable.persistentListOf
import kotlin.math.sign

data class Point(public val x: Int, public val y: Int) {
    fun inDirection(d: Direction): Point = when (d) {
        Direction.Up -> Point(x, y + 1)
        Direction.Down -> Point(x, y - 1)
        Direction.Left -> Point(x - 1, y)
        Direction.Right -> Point(x + 1, y)
    }

    fun isTouching(other: Point): Boolean =
        Math.abs(x - other.x) <= 1 && Math.abs(y - other.y) <= 1

    fun towardsPoint(other: Point): Point = Point(x + (other.x - x).sign, y + (other.y - y).sign)
}

class State(
    private val knots: List<Point>,
    private val tailVisits: PersistentSet<Point>
) {
    fun apply(command: Command): State = (1..command.amount).fold(this) { prev, _ ->
        val newKnots = prev.knots.fold(persistentListOf<Point>()) { prevKnots, knot ->
            val mostRecentKnot = prevKnots.lastOrNull()
            val newPos = if (mostRecentKnot == null) {
                knot.inDirection(command.direction)
            } else if (mostRecentKnot.isTouching(knot)) {
                knot
            } else {
                knot.towardsPoint(mostRecentKnot)
            }
            prevKnots.add(newPos)
        }
        State(newKnots, prev.tailVisits.add(newKnots.last()))
    }

    fun getVisitedCount(): Int = tailVisits.size
}

enum class Direction {
    Up,
    Down,
    Left,
    Right,
}

data class Command(public val direction: Direction, public val amount: Int)

fun parseLine(line: String): Command {
    val parts = line.split(" ")
    val direction = when(parts[0]) {
        "U" -> Direction.Up
        "D" -> Direction.Down
        "L" -> Direction.Left
        "R" -> Direction.Right
        else -> throw Exception("Unknown direction: ${parts[0]}")
    }

    return Command(direction, parts[1].toInt())
}

fun main(args: Array<String>) {
    val start = Point(0, 0)
    val knots = listOf(start, start, start, start, start, start, start, start, start, start)
    val finalState = File(args.first()).useLines { lines ->
        lines.fold(State(knots, persistentSetOf(start))) { prev, curr ->
            prev.apply(parseLine(curr))
        }
    }

    println(finalState.getVisitedCount())
}
