package app

import java.io.File

sealed interface Line

data class CdCommand(public val target: String) : Line {}
class LsCommand : Line {}

data class FileListing(public val size: Int) : Line {}

class Dir : Line {
    public var size = 0

    fun add(num: Int) {
        size += num
    }
}

fun parseLine(line: String): Line {
    val parts = line.split("""\s+""".toRegex())
    return if (parts[0] == "$") {
        when (parts[1]) {
            "ls" -> LsCommand()
            "cd" -> CdCommand(parts[2])
            else -> throw Error("Unknown command: ${parts[1]}")
        }
    } else if ("""\d+""".toRegex().matchEntire(parts[0]) != null) {
        FileListing(parts[0].toInt())
    } else if (parts[0] == "dir") {
        Dir()
    } else {
        throw Error("Unknown line: $line")
    }
}

fun main(args: Array<String>) {
    var total = 0
    var dirs = mutableListOf<Dir>()
    File(args.first()).forEachLine { line ->
        val parsed = parseLine(line)
        when(parsed) {
            is CdCommand ->
                when(parsed.target) {
                    ".." -> {
                        val mostRecent = dirs.removeLast()
                        dirs.last().add(mostRecent.size)
                        if (mostRecent.size <= 100000) {
                            total += mostRecent.size
                        }
                    }
                    else -> dirs.add(Dir())
                }
            is FileListing -> dirs.last().add(parsed.size)
            is Dir -> {}
            is LsCommand -> {}
        }
    }

    val mostRecent = dirs.removeLast()
    if (mostRecent.size <= 100000) {
        total += mostRecent.size
    }

    println(total)
}
