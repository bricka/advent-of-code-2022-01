package app

import java.io.File

sealed interface Line

data class CdCommand(public val target: String) : Line {}
class LsCommand : Line {}

data class FileListing(public val size: Int) : Line {}

class DirListing : Line {}

class Dir(parentPath: String?, name: String) {
    public val path = if (parentPath == null) {
        name
    } else if (parentPath == "/") {
        "/$name"
    } else {
        "$parentPath/$name"
    }
    public var size = 0

    fun add(num: Int) {
        size += num
    }

    override fun toString() = "Dir[$path ($size)]"
}

fun parseLine(line: String): Line {
    val parts = line.split("""\s+""".toRegex())
    return if (parts[0] == "$") {
        when (parts[1]) {
            "ls" -> LsCommand()
            "cd" -> CdCommand(parts[2])
            else -> throw Error("Unknown command: ${parts[1]}")
        }
    } else if ("""\d+""".toRegex().matchEntire(parts[0]) != null) {
        FileListing(parts[0].toInt())
    } else if (parts[0] == "dir") {
        DirListing()
    } else {
        throw Error("Unknown line: $line")
    }
}

fun main(args: Array<String>) {
    val allDirectories = mutableSetOf<Dir>()
    var dirs = mutableListOf<Dir>()
    File(args.first()).forEachLine { line ->
        val parsed = parseLine(line)
        when(parsed) {
            is CdCommand ->
                when(parsed.target) {
                    ".." -> {
                        val mostRecent = dirs.removeLast()
                        allDirectories.add(mostRecent)
                        dirs.last().add(mostRecent.size)
                    }
                    else -> dirs.add(Dir(dirs.lastOrNull()?.path, parsed.target))
                }
            is FileListing -> dirs.last().add(parsed.size)
            is DirListing -> {}
            is LsCommand -> {}
        }
    }

    while (!dirs.isEmpty()) {
        val mostRecent = dirs.removeLast()
        allDirectories.add(mostRecent)
        dirs.lastOrNull()?.add(mostRecent.size)
    }

    val totalSize = allDirectories.maxOf { it.size }
    val unusedSpace = 70000000 - totalSize
    val needToRemove = 30000000 - unusedSpace
    println(allDirectories.filter { it.size >= needToRemove }.minOf { it.size })
}
