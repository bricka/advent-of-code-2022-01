package app

import java.io.File

class Tree(public val height: Int) {
    public var visible = false
}

typealias Trees = Array<Array<Tree>>

sealed class Sequence {
    abstract fun startValue(heights: Trees): Int
    abstract fun stopValue(heights: Trees): Int
    abstract fun adjust(current: Int): Int

    object LowToHigh: Sequence() {
        override fun startValue(heights: Trees) = 0
        override fun stopValue(heights: Trees) = heights.size
        override fun adjust(current: Int) = current + 1
    }

    object HighToLow: Sequence() {
        override fun startValue(heights: Trees) = heights.size - 1
        override fun stopValue(heights: Trees) = -1
        override fun adjust(current: Int) = current - 1
    }
}

fun Trees.markVisibleTreesRowFirst(rowSequence: Sequence, colSequence: Sequence) {
    println("--")
    var i = rowSequence.startValue(this)

    while (i != rowSequence.stopValue(this)) {
        println("-")
        var biggestSoFar = -1
        var j = colSequence.startValue(this)
        while (j != colSequence.stopValue(this)) {
            val tree = this[i][j]

            if (tree.height > biggestSoFar) {
                println("Visible: ${tree.height}")
                tree.visible = true
                biggestSoFar = tree.height
            }
            j = colSequence.adjust(j)
        }
        i = rowSequence.adjust(i)
    }
}

fun Trees.markVisibleTreesColFirst(rowSequence: Sequence, colSequence: Sequence) {
    println("--")
    var i = rowSequence.startValue(this)

    while (i != rowSequence.stopValue(this)) {
        println("-")
        var biggestSoFar = -1
        var j = colSequence.startValue(this)
        while (j != colSequence.stopValue(this)) {
            val tree = this[j][i]

            if (tree.height > biggestSoFar) {
                println("Visible: ${tree.height}")
                tree.visible = true
                biggestSoFar = tree.height
            }
            j = colSequence.adjust(j)
        }
        i = rowSequence.adjust(i)
    }
}
fun Trees.getVisibleTreeCount(): Int = this.fold(0) { prev, curr -> prev + curr.filter { it.visible }.size }

fun heightsToString(heights: Trees): String =
    heights.fold("") { colPrev, col ->
        colPrev + col.fold("") { rowPrev, tree ->
            rowPrev + tree.height
        } + "\n"
    }

fun getAllTreesFromFile(file: File): Trees {
    val processedLines = mutableListOf<Array<Tree>>()
    file.forEachLine() { line ->
        val lineSoFar = mutableListOf<Tree>()
        for (c in line) {
            lineSoFar.add(Tree(c.digitToInt()))
        }
        processedLines.add(lineSoFar.toTypedArray())
    }

    return processedLines.toTypedArray()
}

fun main(args: Array<String>) {
    val trees = getAllTreesFromFile(File(args.first()))
    trees.markVisibleTreesRowFirst(Sequence.LowToHigh, Sequence.LowToHigh)
    trees.markVisibleTreesRowFirst(Sequence.LowToHigh, Sequence.HighToLow)
    // trees.markVisibleTreesRowFirst(Sequence.HighToLow, Sequence.LowToHigh)
    // trees.markVisibleTreesRowFirst(Sequence.HighToLow, Sequence.HighToLow)
    trees.markVisibleTreesColFirst(Sequence.LowToHigh, Sequence.LowToHigh)
    trees.markVisibleTreesColFirst(Sequence.LowToHigh, Sequence.HighToLow)
    // trees.markVisibleTreesColFirst(Sequence.HighToLow, Sequence.LowToHigh)
    // trees.markVisibleTreesColFirst(Sequence.HighToLow, Sequence.HighToLow)

    println(trees.getVisibleTreeCount())
}
