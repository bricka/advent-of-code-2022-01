package app

import kotlin.test.assertEquals
import kotlinx.collections.immutable.persistentMapOf
import org.junit.jupiter.api.Test

internal class ViewTrackerTest {
    @Test
    fun `it returns correct distances if nothing has been encountered`() {
        val viewTracker = ViewTracker(persistentMapOf())
        for (height in 0..9) {
            assertEquals(0, viewTracker.getViewingDistanceForHeight(height))
        }
    }

    @Test
    fun `it returns correct distances if a tree has been encountered`() {
        val viewTracker = ViewTracker(persistentMapOf()).encounterTree(4)
        for (height in 0..9) {
            assertEquals(1, viewTracker.getViewingDistanceForHeight(height), "Trees for $height should be 1")
        }
    }

    @Test
    fun `it returns different trees for different heights`() {
        val viewTracker = ViewTracker(persistentMapOf()).encounterTree(7).encounterTree(4)
        assertEquals(1, viewTracker.getViewingDistanceForHeight(0))
        assertEquals(1, viewTracker.getViewingDistanceForHeight(1))
        assertEquals(1, viewTracker.getViewingDistanceForHeight(2))
        assertEquals(1, viewTracker.getViewingDistanceForHeight(3))
        assertEquals(1, viewTracker.getViewingDistanceForHeight(4))
        assertEquals(2, viewTracker.getViewingDistanceForHeight(5))
        assertEquals(2, viewTracker.getViewingDistanceForHeight(6))
        assertEquals(2, viewTracker.getViewingDistanceForHeight(7))
        assertEquals(2, viewTracker.getViewingDistanceForHeight(8))
        assertEquals(2, viewTracker.getViewingDistanceForHeight(9))
    }
}
