package app

import kotlinx.collections.immutable.persistentMapOf
import java.io.File

class Tree(public val height: Int) {
    private val viewingDistances = mutableListOf<Int>()

    fun addViewingDistance(distance: Int) {
        viewingDistances.add(distance)
    }

    fun getScenicScore(): Int = viewingDistances.fold(1) { prev, curr -> prev * curr }
}

typealias Trees = Array<Array<Tree>>

fun Trees.calculateDistances(rowRange: IntProgression, colRange: IntProgression, rowFirst: Boolean) {
    for (i in rowRange) {
        var viewTracker = ViewTracker(persistentMapOf())
        for (j in colRange) {
            println("$i,$j")
            val tree = if (rowFirst) { this[i][j] } else { this[j][i] }
            // println(treeStringMarking(this, tree))
            val viewingDistance = viewTracker.getViewingDistanceForHeight(tree.height)
            println("Viewing distance: $viewingDistance")
            tree.addViewingDistance(viewingDistance)
            viewTracker = viewTracker.encounterTree(tree.height)
        }
    }
}

fun treeStringMarking(forest: Trees, tree: Tree): String =
    forest.fold("") { colPrev, col ->
        colPrev + col.fold("") { rowPrev, curr ->
            rowPrev + if (curr == tree) { "[${curr.height}]" } else { " ${curr.height} " }
        } + "\n"
    }

fun heightsToString(heights: Trees): String =
    heights.fold("") { colPrev, col ->
        colPrev + col.fold("") { rowPrev, tree ->
            rowPrev + tree.height
        } + "\n"
    }

fun getAllTreesFromFile(file: File): Trees {
    val processedLines = mutableListOf<Array<Tree>>()
    file.forEachLine() { line ->
        val lineSoFar = mutableListOf<Tree>()
        for (c in line) {
            lineSoFar.add(Tree(c.digitToInt()))
        }
        processedLines.add(lineSoFar.toTypedArray())
    }

    return processedLines.toTypedArray()
}

fun main(args: Array<String>) {
    val trees = getAllTreesFromFile(File(args.first()))
    val max = trees.size - 1
    trees.calculateDistances(0..max, 0..max, true)
    trees.calculateDistances(0..max, max.downTo(0), true)
    trees.calculateDistances(0..max, 0..max, false)
    trees.calculateDistances(0..max, max.downTo(0), false)

    println(trees.flatMap { it.toList() }.maxOf { it.getScenicScore() })
}
