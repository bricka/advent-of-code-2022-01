package app

import kotlinx.collections.immutable.PersistentMap

public class ViewTracker(private val map: PersistentMap<Int, Int>) {
    fun encounterTree(height: Int): ViewTracker {
        val shorter = (0..height).fold(map) { prev, curr -> prev.put(curr, 1) }
        val higher = ((height+1)..9).fold(shorter) { prev, curr -> prev.put(curr, (prev.get(curr) ?: 0) + 1) }
        return ViewTracker(higher)
    }

    fun getViewingDistanceForHeight(height: Int): Int = map.get(height) ?: 0
}
