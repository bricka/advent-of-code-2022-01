package app

import java.io.File

val MAX_MINUTE = 30

val LINE_REGEX = """Valve (\w+) has flow rate=(\d+); tunnels? leads? to valves? (.*)""".toRegex()

data class Valve(val flowRate: ULong?, val next: Set<String>)

fun parseLines(lines: List<String>): Map<String, Valve> {
    val valveById = mutableMapOf<String, Valve>()
    lines.forEach { line ->
        val (id, flowRateString, nextString) = LINE_REGEX.matchEntire(line)!!.destructured
        val flowRate = when (flowRateString) {
            "0" -> null
            else -> flowRateString.toULong()
        }
        valveById.put(id, Valve(flowRate, nextString.split(", ").toSet()))
    }

    return valveById
}

data class State(
    val minute: Int,
    val position: String,
    val openValves: Set<String>,
    val pressureReleased: ULong,
) {
    fun openValve(valveById: Map<String, Valve>, valve: String): State {
        val newPressureReleased = calculateNewFlowRate(valveById)
        val newOpenValves = openValves.toMutableSet()
        newOpenValves.add(valve)
        return State(minute + 1, position, newOpenValves, newPressureReleased)
    }

    fun moveTo(valveById: Map<String, Valve>, destination: String): State {
        val newPressureReleased = calculateNewFlowRate(valveById)
        return State(minute + 1, destination, openValves, newPressureReleased)
    }

    private fun calculateNewFlowRate(valveById: Map<String, Valve>): ULong =
        openValves.fold(pressureReleased) { prev, curr -> prev + valveById.get(curr)!!.flowRate!! }
}

fun main(args: Array<String>) {
    val valveById = parseLines(File(args.first()).readLines())
    val startState = State(0, "AA", setOf(), 0UL)
    val seenStates = mutableSetOf<State>()
    val frontier = ArrayDeque<State>()
    frontier.add(startState)

    while (!frontier.isEmpty()) {
        val next = frontier.removeFirst()
        if (seenStates.contains(next)) {
            continue
        }
        seenStates.add(next)

        if (next.minute == MAX_MINUTE) {
            continue
        }

        val position = next.position
        val valve = valveById.get(position)!!
        if (valve.flowRate != null && !next.openValves.contains(position)) {
            frontier.addLast(next.openValve(valveById, next.position))
        }

        frontier.addAll(valve.next.map { next.moveTo(valveById, it) })
    }

    println(seenStates.map { it.pressureReleased }.maxOrNull())
}
