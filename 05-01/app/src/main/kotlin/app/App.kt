package app

import java.io.File
import java.nio.charset.Charset

val instructionRegex = """move (\d+) from (\d+) to (\d+)""".toRegex()

class State {
    private val map: MutableMap<Int, ArrayDeque<Char>> = mutableMapOf()

    fun addValueToBottom(stack: Int, value: Char) {
        println("Putting $value in $stack")
        var queue = map.get(stack)
        if (queue == null) {
            queue = ArrayDeque()
            map.put(stack, queue)
        }
        queue.addFirst(value)
    }

    fun move(count: Int, source: Int, target: Int) {
        (0..count-1).forEach { _ ->
            map.get(target)!!.addLast(map.get(source)!!.removeLast())
        }
    }

    fun getPeaks(): List<Char> {
        return map.keys.sorted().map { map.get(it)!!.last() }
    }

    override fun toString(): String {
        val lastStack = map.keys.maxOf { it }
        val maxHeight = map.values.maxOf { it.size }
        // println("stackNumbers: $stackNumbers, maxHeight $maxHeight")

        val stackContents = (maxHeight-1).downTo(0).map { height ->
            (1..lastStack).map { stackNumber ->
                val value = map.get(stackNumber)?.getOrNull(height)
                // println("$stackNumber@$height = $value")
                if (value == null) {
                    "   "
                } else {
                    "[$value]"
                }
            }.reduce { prev, curr -> prev + " $curr" }
        }.reduce { prev, curr -> prev + "\n$curr" }

        return stackContents + "\n" + (1..lastStack).fold("") { prev, curr -> prev + " $curr  "}
    }
}

fun constructInitialState(lines: Sequence<String>): State {
    val initialState = State()
    lines.forEach { line ->
        println("line: $line")
        var stackNumber = 1
        var startIndex = 0
        while (startIndex < line.length) {
            var substr = line.substring(startIndex, startIndex + 3)
            println("substr: $substr")
            if (!substr.isBlank()) {
                initialState.addValueToBottom(stackNumber, substr.get(1))
            }

            stackNumber++
            startIndex += 4
        }
    }
    return initialState
}

fun main(args: Array<String>) {
    val initialFilename = args.first()
    val instructionsFilename = args.get(1)
    val state = File(initialFilename).useLines(Charset.defaultCharset(), ::constructInitialState)

    File(instructionsFilename).forEachLine() { line ->
        val (countString, sourceString, targetString) = instructionRegex.matchEntire(line)!!.destructured
        val count = countString.toInt()
        val source = sourceString.toInt()
        val target = targetString.toInt()

        state.move(count, source, target)
    }

    println(state)
    println(state.getPeaks().joinToString(""))
}
