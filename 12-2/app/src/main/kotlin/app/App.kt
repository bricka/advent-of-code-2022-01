package app

import java.io.File
import kotlinx.collections.immutable.PersistentSet
import kotlinx.collections.immutable.persistentSetOf

data class ParseResult(val end: Point, val heightMap: HeightMap)

fun parseFile(lines: List<String>): ParseResult {
    var end: Point? = null
    var map: MutableMap<Point, Int> = mutableMapOf()

    for ((row, line) in lines.reversed().withIndex()) {
        for ((col, char) in line.withIndex()) {
            val point = Point(col, row)
            when(char) {
                'S' -> map.put(point, 0)
                'E' -> {
                    end = point
                    map.put(point, 25)
                }
                else -> map.put(point, char - 'a')
            }
        }
    }

    return ParseResult(end!!, HeightMap(map))
}

data class Path(val current: Point, val length: Int)

tailrec fun lengthToEnd(visited: PersistentSet<Point>, heightMap: HeightMap, end: Point, frontier: PersistentSet<Path>): Int {
    // println("$visited, $frontier")
    val bestCandidate = frontier.minByOrNull { it.length }
    if (bestCandidate == null) {
        return 10000
    }
    val (current, length) = bestCandidate
    // println("bestCandidate = ${current}, ${length}}")
    if (visited.contains(current)) {
        return lengthToEnd(visited, heightMap, end, frontier.remove(bestCandidate))
    }
    val neighbors = heightMap.getNeighborsOf(current).filter { !visited.contains(it) }.filter { heightMap.getHeightAtPoint(it)!! <= heightMap.getHeightAtPoint(current)!! + 1 }
    if (neighbors.isEmpty()) {
        return lengthToEnd(visited.add(current), heightMap, end, frontier.remove(bestCandidate))
    }

    if (neighbors.any { it == end }) {
        return length + 1
    }

    return lengthToEnd(visited.add(current), heightMap, end, frontier.remove(bestCandidate).addAll(neighbors.map { Path(it, length + 1)}))
}

fun main(args: Array<String>) {
    val (end, heightMap) = parseFile(File(args.first()).readLines())
    // println(start)
    // println(end)
    // println(heightMap)
    val bestRouteLength = heightMap.getLowestPoints().map { lengthToEnd(persistentSetOf(), heightMap, end, persistentSetOf(Path(it, 0))) }.minOrNull()
    println(bestRouteLength)
}
