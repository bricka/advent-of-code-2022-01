package app

data class Point(public val x: Int, public val y: Int)

private fun heightToLetter(height: Int): Char = 'a' + height

class HeightMap(private val pointToHeight: Map<Point, Int>) {
    fun getHeightAtPoint(point: Point): Int? = pointToHeight.get(point)
    fun getNeighborsOf(point: Point): Set<Point> = setOf(Point(point.x, point.y + 1), Point(point.x, point.y - 1), Point(point.x - 1, point.y), Point(point.x + 1, point.y)).filter { pointToHeight.get(it) != null }.toSet()
    fun getLowestPoints(): Set<Point> = pointToHeight.entries.filter { it.value == 0 }.map { it.key }.toSet()

    override fun toString() =
        pointToHeight.keys.map { it.y }.distinct().sortedDescending().fold("") { linesSoFar, y ->
            linesSoFar + "\n" + pointToHeight.keys.map { it.x }.distinct().sorted().fold("") { rowSoFar, x -> rowSoFar + heightToLetter(pointToHeight.get(Point(x, y)) ?: 27) }
        }
}
