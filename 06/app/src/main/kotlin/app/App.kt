package app

import java.io.File

class CircularBuffer(size: Int) {
    private val buf = CharArray(size)
    private var isFull = false
    private var index = 0

    fun push(char: Char) {
        buf[index++] = char
        if (index == buf.size) {
            isFull = true
            index = 0
        }
    }

    fun isFullAndDistinct(): Boolean = isFull && buf.toSet().size == buf.size
}

fun main(args: Array<String>) {
    val datastream = File(args.first()).readLines().first()

    val buf = CircularBuffer(14) // Use 4 for Day 1

    for ((index, value) in datastream.withIndex()) {
        buf.push(value)
        if (buf.isFullAndDistinct()) {
            println(index + 1)
            return
        }
    }

    println(datastream)
}
