package app

import java.io.File
import kotlin.math.sign

class Grid(private val width: Int, private val height: Int) {
    private val grid = Array(width) { Array(height) { false } }

    fun markFull(p: Point) {
        grid[p.x][p.y] = true
    }

    fun isFull(p: Point): Boolean = grid[p.x][p.y]

    fun highestFullPointBelow(p: Point): Point? {
        for(y in (p.y+1..height-1)) {
            if (grid[p.x][y]) {
                return Point(p.x, y)
            }
        }

        return null
    }
}

data class Point(val x: Int, val y: Int) {
    fun towards(other: Point): Point = if (x == other.x) {
        Point(x, y + (other.y - y).sign)
    } else {
        Point(x + (other.x - x).sign, y)
    }

    fun up(): Point = Point(x, y - 1)
    fun downLeft(): Point = Point(x - 1, y + 1)
    fun downRight(): Point = Point(x + 1, y + 1)

    override fun toString() = "($x, $y)"
}

data class LineSegment(val guidePoints: List<Point>) {
    fun points(): List<Point> {
        var lastPoint = guidePoints.first()
        val points = mutableListOf(lastPoint)

        for (nextPoint in guidePoints.subList(1, guidePoints.size)) {
            while(lastPoint != nextPoint) {
                lastPoint = lastPoint.towards(nextPoint)
                points.add(lastPoint)
            }
        }

        return points
    }

    override fun toString() = guidePoints.joinToString(" -> ")
}

fun parseInput(lines: List<String>): List<LineSegment> {
    val list = mutableListOf<LineSegment>()

    for (line in lines) {
        list.add(LineSegment(line.split(" -> ").map { it.split(",") }.map { Point(it.get(0).toInt(), it.get(1).toInt()) }))
    }

    return list
}

fun dropSandUntilAbyss(grid: Grid): Int {
    var droppedSand = 0

    newsand@ while(true) {
        droppedSand++

        var dropPoint = Point(500, 0)
        while (true) {
            // println("Dropping from $dropPoint")
            val whatItHits = grid.highestFullPointBelow(dropPoint)
            if (whatItHits == null) {
                return droppedSand - 1
            }
            val landsAt = whatItHits.up()
            // println("Landed at $landsAt")
            if (grid.isFull(landsAt.downLeft()) && grid.isFull(landsAt.downRight())) {
                grid.markFull(landsAt)
                continue@newsand
            }
            if (!grid.isFull(landsAt.downLeft())) {
                dropPoint = landsAt.downLeft()
            } else {
                dropPoint = landsAt.downRight()
            }
        }
    }
}

fun main(args: Array<String>) {
    val lineSegments = parseInput(File(args.first()).readLines())
    val points = lineSegments.flatMap { it.guidePoints }
    val width = points.map { it.x }.maxOrNull()!! + 1
    val height = points.map { it.y }.maxOrNull()!! + 1

    val grid = Grid(width, height)
    lineSegments.flatMap { it.points() }.forEach{ it -> grid.markFull(it) }

    println(dropSandUntilAbyss(grid))
}
