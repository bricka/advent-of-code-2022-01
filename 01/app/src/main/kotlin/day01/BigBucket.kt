package day01

import java.util.HashSet

class BigBucket constructor(private val size: Int) {
    private val nums: MutableList<Int> = mutableListOf()

    fun add(number: Int) {
        if (nums.size < size) {
            nums.add(number)
            return
        }

        val lowestInSet = nums.minOf { it }
        if (lowestInSet < number) {
            nums.remove(lowestInSet)
            nums.add(number)
        }
    }

    fun sum(): Int {
        return nums.sumOf { it }
    }
}
