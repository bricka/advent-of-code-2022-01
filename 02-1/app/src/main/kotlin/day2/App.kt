package day2

import java.io.File

enum class Result {
    Beats,
    Loses,
    Tie
}

sealed class Shape(val points: Int) {
    abstract val beats: Shape

    fun result(other: Shape): Result {
        if (this == other) {
            return Result.Tie
        } else if (this.beats == other) {
            return Result.Beats
        } else {
            return Result.Loses
        }
    }

    object Rock: Shape(1) {
        override val beats = Scissors
    }

    object Paper: Shape(2) {
        override val beats = Rock
    }

    object Scissors: Shape(3) {
        override val beats = Paper
    }
}

data class Round(val opponent: Shape, val me: Shape) {
    fun getPoints(): Int {
        val pointsForWinning = when(me.result(opponent)) {
            Result.Beats -> 6
            Result.Loses -> 0
            Result.Tie -> 3
        }

        return pointsForWinning + me.points
    }
}

fun shapeForOpponent(code: String): Shape {
    return when(code) {
        "A" -> Shape.Rock
        "B" -> Shape.Paper
        "C" -> Shape.Scissors
        else -> throw Exception("Invalid opponent shape: $code")
    }
}

fun shapeForMe(code: String): Shape {
    return when(code) {
        "X" -> Shape.Rock
        "Y" -> Shape.Paper
        "Z" -> Shape.Scissors
        else -> throw Exception("Invalid me shape: $code")
    }
}

fun roundFromLine(line: String): Round {
    val (opponent, me) = line.split(" ")
    return Round(shapeForOpponent(opponent), shapeForMe(me))
}

fun main(args: Array<String>) {
    val inputFilename = args.firstOrNull()
    if (inputFilename == null) {
        throw Exception("Must provide an input filename")
    }

    val points = File(inputFilename).useLines { lines ->
        lines.fold(0) { prev, curr -> prev + roundFromLine(curr).getPoints() }
    }

    println(points)
}
