package app

import java.io.File
import kotlinx.collections.immutable.PersistentSet
import kotlinx.collections.immutable.persistentSetOf
import kotlin.math.sign

data class Point(public val x: Int, public val y: Int) {
    fun inDirection(d: Direction): Point = when (d) {
        Direction.Up -> Point(x, y + 1)
        Direction.Down -> Point(x, y - 1)
        Direction.Left -> Point(x - 1, y)
        Direction.Right -> Point(x + 1, y)
    }

    fun isTouching(other: Point): Boolean =
        Math.abs(x - other.x) <= 1 && Math.abs(y - other.y) <= 1

    fun towardsPoint(other: Point): Point = Point(x + (other.x - x).sign, y + (other.y - y).sign)
}

class State(
    private val head: Point,
    private val tail: Point,
    private val tailVisits: PersistentSet<Point>
) {
    fun apply(command: Command): State = (1..command.amount).fold(this) { prev, _ ->
        val newHead = prev.head.inDirection(command.direction)
        val newTail = if (newHead.isTouching(prev.tail)) {
            prev.tail
        } else {
            prev.tail.towardsPoint(newHead)
        }
        State(newHead, newTail, prev.tailVisits.add(newTail))
    }

    fun getVisitedCount(): Int = tailVisits.size
}

enum class Direction {
    Up,
    Down,
    Left,
    Right,
}

data class Command(public val direction: Direction, public val amount: Int)

fun parseLine(line: String): Command {
    val parts = line.split(" ")
    val direction = when(parts[0]) {
        "U" -> Direction.Up
        "D" -> Direction.Down
        "L" -> Direction.Left
        "R" -> Direction.Right
        else -> throw Exception("Unknown direction: ${parts[0]}")
    }

    return Command(direction, parts[1].toInt())
}

fun main(args: Array<String>) {
    val start = Point(0, 0)
    val finalState = File(args.first()).useLines { lines ->
        lines.fold(State(start, start, persistentSetOf(start))) { prev, curr ->
            prev.apply(parseLine(curr))
        }
    }

    println(finalState.getVisitedCount())
}
