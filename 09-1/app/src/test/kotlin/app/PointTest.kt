package app

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

public class PointTest {
    @Test
    fun `inDirection can move in a direction`() {
        val p = Point(0, 0)
        assertEquals(Point(1, 0), p.inDirection(Direction.Right))
    }

    @Test
    fun `isTouching is touching itself`() {
        assertEquals(true, Point(0, 0).isTouching(Point(0, 0)))
    }

    @Test
    fun `isTouching is touching direct neighbor`() {
        assertEquals(true, Point(0, 0).isTouching(Point(0, 1)))
    }

    @Test
    fun `isTouching is touching diagonal neighbor`() {
        assertEquals(true, Point(0, 0).isTouching(Point(1, 1)))

    }

    @Test
    fun `isTouching is not touching neighbor too far away`() {
        assertEquals(false, Point(0, 0).isTouching(Point(0, 2)))
    }

    @Test
    fun `isTouching is not touching neighbor too far away on diagonal`() {
        assertEquals(false, Point(0, 0).isTouching(Point(2, 2)))
    }

    @Test
    fun `towardsPoint moves towards same X`() {
        val tail = Point(0, 0)
        val head = Point(2, 0)
        val newTail = Point(1, 0)

        assertEquals(newTail, tail.towardsPoint(head))
    }

    @Test
    fun `towardsPoint moves towards same Y`() {
        val tail = Point(0, 0)
        val head = Point(0, 2)
        val newTail = Point(0, 1)

        assertEquals(newTail, tail.towardsPoint(head))
    }

    @Test
    fun `towardsPoint moves on diagonal`() {
        val tail = Point(0, 0)
        val head = Point(2, 1)
        val newTail = Point(1, 1)

        assertEquals(newTail, tail.towardsPoint(head))
    }
}
