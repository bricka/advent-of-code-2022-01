package app

import java.io.File
import kotlinx.collections.immutable.PersistentList
import kotlinx.collections.immutable.PersistentMap
import kotlinx.collections.immutable.persistentListOf
import kotlinx.collections.immutable.persistentMapOf

class Monkey(
    private val items: PersistentList<Int>,
    private val operation: Operation,
    private val test: Test,
    private val trueTarget: Int,
    private val falseTarget: Int,
    public val timesThrown: Int,
)   {
    override fun toString(): String =
        "Inspected $timesThrown times\nItems: $items\nOperation: $operation\nTest: $test\nIf true: throw to monkey $trueTarget\nIf false: throw to monkey $falseTarget"

    fun addItems(newItems: List<Int>): Monkey = Monkey(
        items.addAll(newItems),
        operation,
        test,
        trueTarget,
        falseTarget,
        timesThrown,
    )

    fun takeTurn(): Pair<Monkey, Map<Int, List<Int>>> {
        val thrownItems = items.fold(persistentMapOf<Int, PersistentList<Int>>()) { prev, curr ->
            val currentWorryLevel = operation.perform(curr) / 3
            if (test.passesTest(currentWorryLevel)) {
                prev.put(trueTarget, prev.get(trueTarget)?.add(currentWorryLevel) ?: persistentListOf(currentWorryLevel))
            } else {
                prev.put(falseTarget, prev.get(falseTarget)?.add(currentWorryLevel) ?: persistentListOf(currentWorryLevel))
            }
        }

        return Pair(
            Monkey(persistentListOf(), operation, test, trueTarget, falseTarget, timesThrown + items.size),
            thrownItems
        )
    }
}

class Operation(incomingOperand1: String, incomingOperator: String, incomingOperand2: String) {
    private val operand1 = operandFor(incomingOperand1)
    private val operand2 = operandFor(incomingOperand2)
    private val operator = operatorFor(incomingOperator)

    override fun toString(): String = "$operand1 $operator $operand2"
    fun perform(current: Int): Int = operator(operand1.getValue(current), operand2.getValue(current))
}

class Test(private val divisor: Int) {
    fun passesTest(value: Int): Boolean = value % divisor == 0
    override fun toString(): String = "divisible by $divisor"
}

sealed interface Operand {
    fun getValue(oldValue: Int): Int
}

object OldOperand: Operand {
    override fun getValue(oldValue: Int): Int = oldValue
    override fun toString(): String = "old"
}

class IntOperand(private val value: Int): Operand {
    override fun getValue(oldValue: Int) = value
    override fun toString(): String = value.toString()
}

class State(public val monkeys: PersistentMap<Int, Monkey>) {
    fun addMonkey(monkeyNum: Int, monkey: Monkey) = State(monkeys.put(monkeyNum, monkey))

    override fun toString(): String = monkeys.toString()
}

fun main(args: Array<String>) {
    val initialState = parse(File(args.first()).readLines())

    val finalState = (0..19).fold(initialState) { prevState, _ ->
        val monkeyNums = prevState.monkeys.keys.sorted()
        State(monkeyNums.fold(prevState.monkeys) { prevMonkeys, monkeyNum ->
            val (newMonkey, thrownItems) = prevMonkeys.get(monkeyNum)!!.takeTurn()
            thrownItems.entries.fold(prevMonkeys) { monkeyMap, entry ->
                monkeyMap.put(entry.key, monkeyMap.get(entry.key)!!.addItems(entry.value))
            }.put(monkeyNum, newMonkey)
        })
    }

    println(finalState)
    println(finalState.monkeys.entries.map { it.value.timesThrown }.sortedDescending().take(2).reduce { prev, curr -> prev * curr })
}
