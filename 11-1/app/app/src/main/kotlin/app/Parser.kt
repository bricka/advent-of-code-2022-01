package app

import kotlinx.collections.immutable.toPersistentList
import kotlinx.collections.immutable.persistentMapOf

private val NEW_MONKEY_REGEX = """Monkey (\d+):""".toRegex()
private val STARTING_ITEMS_REGEX = """\s+Starting items: (\d+(, \d+)*)""".toRegex()
private val OPERATION_REGEX = """\s+Operation: new = (old|\d+) (\+|-|\*|/) (old|\d+)""".toRegex()
private val TEST_REGEX = """\s+Test: divisible by (\d+)""".toRegex()
private val TRUE_TARGET_REGEX = """\s+If true: throw to monkey (\d+)""".toRegex()
private val FALSE_TARGET_REGEX = """\s+If false: throw to monkey (\d+)""".toRegex()

fun operandFor(incoming: String): Operand = if (incoming == "old") {
    OldOperand
} else {
    IntOperand(incoming.toInt())
}

fun plus(a: Int, b: Int): Int = a + b
fun minus(a: Int, b: Int): Int = a - b
fun times(a: Int, b: Int): Int = a * b
fun divide(a: Int, b: Int): Int = a / b

fun operatorFor(incoming: String): (a: Int, b: Int) -> Int =
    when (incoming) {
        "+" -> ::plus
        "-" -> ::minus
        "*" -> ::times
        "/" -> ::divide
        else -> throw IllegalArgumentException("Unknown operator: $incoming")
    }

class MonkeyBuilder {
    public var monkeyNumber: Int? = null
    private var items: List<Int> = listOf()
    private var operation: Operation? = null
    private var test: Test? = null
    private var trueTarget: Int? = null
    private var falseTarget: Int? = null

    fun monkeyNumber(num: Int): MonkeyBuilder {
        this.monkeyNumber = num
        return this
    }

    fun startingItems(items: List<Int>): MonkeyBuilder {
        this.items = items
        return this
    }

    fun operation(operation: Operation): MonkeyBuilder {
        this.operation = operation
        return this
    }

    fun test(test: Test): MonkeyBuilder {
        this.test = test
        return this
    }

    fun trueTarget(target: Int): MonkeyBuilder {
        this.trueTarget = target
        return this
    }

    fun falseTarget(target: Int): MonkeyBuilder {
        this.falseTarget = target
        return this
    }

    fun build(): Monkey = Monkey(
        items.toPersistentList(),
        operation ?: throw Exception("No operation defined"),
        test ?: throw Exception("No test defined"),
        trueTarget ?: throw Exception("No true target defined"),
        falseTarget ?: throw Exception("No false target defined"),
        0,
    )
}

private data class ParseState(val state: State, val monkeyBuilder: MonkeyBuilder?)

private fun parseNewMonkey(line: String): Int? {
    val matchState = NEW_MONKEY_REGEX.matchEntire(line)
    if (matchState == null) {
        return null
    }

    return matchState.groupValues.get(1).toInt()
}

private fun parseStartingItems(line: String): List<Int>? {
    val startingLineMatchState = STARTING_ITEMS_REGEX.matchEntire(line)
    if (startingLineMatchState == null) {
        return null
    }

    val (itemString) = startingLineMatchState.destructured
    return itemString.split(", ").map(String::toInt)
}

private fun parseOperation(line: String): Operation? {
    val operationMatchState = OPERATION_REGEX.matchEntire(line)
    if (operationMatchState == null) {
        return null
    }
    val (operand1, op, operand2) = operationMatchState.destructured
    return Operation(operand1, op, operand2)
}

private fun parseTest(line: String): Test? {
    val matchData = TEST_REGEX.matchEntire(line)
    if (matchData == null) {
        return null
    }

    return Test(matchData.groupValues.get(1).toInt())
}

private fun parseTrueTarget(line: String): Int? {
    val matchData = TRUE_TARGET_REGEX.matchEntire(line)
    if (matchData == null) {
        return null
    }

    return matchData.groupValues.get(1).toInt()
}

private fun parseFalseTarget(line: String): Int? {
    val matchData = FALSE_TARGET_REGEX.matchEntire(line)
    if (matchData == null) {
        return null
    }

    return matchData.groupValues.get(1).toInt()
}

fun parse(input: List<String>): State {
    val finalParseState = input.fold(ParseState(State(persistentMapOf()), null)) { (state, monkeyBuilder), line ->
        if (line.isBlank()) {
            if (monkeyBuilder != null) {
                return@fold ParseState(state.addMonkey(monkeyBuilder.monkeyNumber ?: throw Exception("No monkey number"), monkeyBuilder.build()), MonkeyBuilder())
            }

            return@fold ParseState(state, monkeyBuilder)
        }

        val newMonkeyNumber = parseNewMonkey(line)
        if (newMonkeyNumber != null) {
            return@fold ParseState(state, MonkeyBuilder().monkeyNumber(newMonkeyNumber))
        }

        val startingItems = parseStartingItems(line)
        if (startingItems != null) {
            return@fold ParseState(state, monkeyBuilder?.startingItems(startingItems))
        }

        val operation = parseOperation(line)
        if (operation != null) {
            return@fold ParseState(state, monkeyBuilder?.operation(operation))
        }

        val test = parseTest(line)
        if (test != null) {
            return@fold ParseState(state, monkeyBuilder?.test(test))
        }

        val trueTarget = parseTrueTarget(line)
        if (trueTarget != null) {
            return@fold ParseState(state, monkeyBuilder?.trueTarget(trueTarget))
        }

        val falseTarget = parseFalseTarget(line)
        if (falseTarget != null) {
            return@fold ParseState(state, monkeyBuilder?.falseTarget(falseTarget))
        }

        throw Exception("Could not parse line: $line")
    }

    return if (finalParseState.monkeyBuilder == null) {
        finalParseState.state
    } else {
        finalParseState.state.addMonkey(finalParseState.monkeyBuilder.monkeyNumber!!, finalParseState.monkeyBuilder.build())
    }
}
