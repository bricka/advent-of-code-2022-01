package day3

import java.io.File
import java.util.stream.Collectors

@JvmInline
value class Item(val c: Char) {
    fun getPriority(): Int = when(c) {
        in 'a'..'z' -> c - 'a' + 1
        in 'A'..'Z' -> c - 'A' + 27
        else -> throw Exception("Invalid item code: $c")
    }
}

class Rucksack(
    private val compartment1: Set<Item>,
    private val compartment2: Set<Item>
) {
    fun findDuplicateItem(): Item? = compartment1.intersect(compartment2).firstOrNull()
}

fun rucksackFromLine(line: String): Rucksack {
    // println("Building rucksack from $line")
    val length = line.length
    val line1 = line.substring(0, length / 2)
    val line2 = line.substring(length / 2)

    return Rucksack(
        line1.toCharArray().map { c -> Item(c.toChar()) }.toSet(),
        line2.toCharArray().map { c -> Item(c.toChar()) }.toSet(),
    )
}

fun main(args: Array<String>) {
    val inputFilename = args.firstOrNull()
    if (inputFilename == null) {
        throw Exception("Must provide an input filename")
    }

    val prioritySum = File(inputFilename).useLines() { lines ->
        lines.fold(0) { runningPriority, line ->
            val rucksack = rucksackFromLine(line)
            val dupe = rucksack.findDuplicateItem()
            if (dupe == null) {
                throw Exception("No dupe found")
            }
            // println("Dupe is ${dupe.c}, prio ${dupe.getPriority()}")
            runningPriority + dupe.getPriority()
        }
    }

    println(prioritySum)
}
