package app

import java.io.File
import java.math.BigInteger

class Monkey(
    private val items: MutableList<BigInteger>,
    private val operation: Operation,
    public val test: Test,
    private val trueTarget: Int,
    private val falseTarget: Int,
    public var timesThrown: BigInteger,
)   {
    override fun toString(): String =
        "Inspected $timesThrown times\nItems: $items\nOperation: $operation\nTest: $test\nIf true: throw to monkey $trueTarget\nIf false: throw to monkey $falseTarget"

    fun addItem(newItem: BigInteger) {
        items.add(newItem)
    }

    fun takeTurn(monkeys: Map<Int, Monkey>, commonDivisor: BigInteger) {
        for (item in items) {
            val currentWorryLevel = operation.perform(item)
            if (test.passesTest(currentWorryLevel)) {
                monkeys.get(trueTarget)!!.addItem(currentWorryLevel % commonDivisor)
            } else {
                monkeys.get(falseTarget)!!.addItem(currentWorryLevel % commonDivisor)
            }
        }

        timesThrown += items.size.toBigInteger()
        items.clear()
    }
}

class Operation(incomingOperand1: String, incomingOperator: String, incomingOperand2: String) {
    private val operand1 = operandFor(incomingOperand1)
    private val operand2 = operandFor(incomingOperand2)
    private val operator = operatorFor(incomingOperator)

    override fun toString(): String = "$operand1 $operator $operand2"
    fun perform(current: BigInteger): BigInteger = operator(operand1.getValue(current), operand2.getValue(current))
}

class Test(public val divisor: BigInteger) {
    fun passesTest(value: BigInteger): Boolean = value.mod(divisor) == BigInteger.ZERO
    override fun toString(): String = "divisible by $divisor"
}

sealed interface Operand {
    fun getValue(oldValue: BigInteger): BigInteger
}

object OldOperand: Operand {
    override fun getValue(oldValue: BigInteger): BigInteger = oldValue
    override fun toString(): String = "old"
}

class IntOperand(private val value: BigInteger): Operand {
    override fun getValue(oldValue: BigInteger) = value
    override fun toString(): String = value.toString()
}

fun main(args: Array<String>) {
    val monkeys = parse(File(args.first()).readLines())
    val monkeyNums = monkeys.keys.sorted()
    val commonDivisor = monkeys.values.map { it.test.divisor }.reduce { curr, prev -> curr * prev }

    repeat(10000) { _ ->
        for (monkeyNum in monkeyNums) {
            monkeys.get(monkeyNum)!!.takeTurn(monkeys, commonDivisor)
        }
    }

    println(monkeys)
    println(monkeys.entries.map { it.value.timesThrown }.sortedDescending().take(2).reduce { prev, curr -> prev * curr })
}
