package app

import java.math.BigInteger

private val NEW_MONKEY_REGEX = """Monkey (\d+):""".toRegex()
private val STARTING_ITEMS_REGEX = """\s+Starting items: (\d+(, \d+)*)""".toRegex()
private val OPERATION_REGEX = """\s+Operation: new = (old|\d+) (\+|-|\*|/) (old|\d+)""".toRegex()
private val TEST_REGEX = """\s+Test: divisible by (\d+)""".toRegex()
private val TRUE_TARGET_REGEX = """\s+If true: throw to monkey (\d+)""".toRegex()
private val FALSE_TARGET_REGEX = """\s+If false: throw to monkey (\d+)""".toRegex()

fun operandFor(incoming: String): Operand = if (incoming == "old") {
    OldOperand
} else {
    IntOperand(incoming.toBigInteger())
}

fun plus(a: BigInteger, b: BigInteger): BigInteger = a + b
fun minus(a: BigInteger, b: BigInteger): BigInteger = a - b
fun times(a: BigInteger, b: BigInteger): BigInteger = a * b
fun divide(a: BigInteger, b: BigInteger): BigInteger = a / b

fun operatorFor(incoming: String): (a: BigInteger, b: BigInteger) -> BigInteger =
    when (incoming) {
        "+" -> ::plus
        "-" -> ::minus
        "*" -> ::times
        "/" -> ::divide
        else -> throw IllegalArgumentException("Unknown operator: $incoming")
    }

class MonkeyBuilder {
    public var monkeyNumber: Int? = null
    private var items: MutableList<BigInteger> = mutableListOf()
    private var operation: Operation? = null
    private var test: Test? = null
    private var trueTarget: Int? = null
    private var falseTarget: Int? = null

    fun monkeyNumber(num: Int): MonkeyBuilder {
        this.monkeyNumber = num
        return this
    }

    fun startingItems(items: MutableList<BigInteger>): MonkeyBuilder {
        this.items = items
        return this
    }

    fun operation(operation: Operation): MonkeyBuilder {
        this.operation = operation
        return this
    }

    fun test(test: Test): MonkeyBuilder {
        this.test = test
        return this
    }

    fun trueTarget(target: Int): MonkeyBuilder {
        this.trueTarget = target
        return this
    }

    fun falseTarget(target: Int): MonkeyBuilder {
        this.falseTarget = target
        return this
    }

    fun build(): Monkey = Monkey(
        items,
        operation ?: throw Exception("No operation defined"),
        test ?: throw Exception("No test defined"),
        trueTarget ?: throw Exception("No true target defined"),
        falseTarget ?: throw Exception("No false target defined"),
        BigInteger.ZERO,
    )
}

private fun parseNewMonkey(line: String): Int? {
    val matchState = NEW_MONKEY_REGEX.matchEntire(line)
    if (matchState == null) {
        return null
    }

    return matchState.groupValues.get(1).toInt()
}

private fun parseStartingItems(line: String): MutableList<BigInteger>? {
    val startingLineMatchState = STARTING_ITEMS_REGEX.matchEntire(line)
    if (startingLineMatchState == null) {
        return null
    }

    val (itemString) = startingLineMatchState.destructured
    return itemString.split(", ").map(String::toBigInteger).toMutableList()
}

private fun parseOperation(line: String): Operation? {
    val operationMatchState = OPERATION_REGEX.matchEntire(line)
    if (operationMatchState == null) {
        return null
    }
    val (operand1, op, operand2) = operationMatchState.destructured
    return Operation(operand1, op, operand2)
}

private fun parseTest(line: String): Test? {
    val matchData = TEST_REGEX.matchEntire(line)
    if (matchData == null) {
        return null
    }

    return Test(matchData.groupValues.get(1).toBigInteger())
}

private fun parseTrueTarget(line: String): Int? {
    val matchData = TRUE_TARGET_REGEX.matchEntire(line)
    if (matchData == null) {
        return null
    }

    return matchData.groupValues.get(1).toInt()
}

private fun parseFalseTarget(line: String): Int? {
    val matchData = FALSE_TARGET_REGEX.matchEntire(line)
    if (matchData == null) {
        return null
    }

    return matchData.groupValues.get(1).toInt()
}

fun parse(input: List<String>): MutableMap<Int, Monkey> {
    val monkeys = mutableMapOf<Int, Monkey>()
    var builder: MonkeyBuilder? = null
    var monkeyNum: Int? = null

    for (line in input) {
        if (line.isBlank()) {
            if (builder != null) {
                monkeys.put(monkeyNum!!, builder.build())
                builder = null
                monkeyNum = null
            }

            continue
        }

        val newMonkeyNumber = parseNewMonkey(line)
        if (newMonkeyNumber != null) {
            monkeyNum = newMonkeyNumber
            builder = MonkeyBuilder()
            continue
        }

        val startingItems = parseStartingItems(line)
        if (startingItems != null) {
            builder?.startingItems(startingItems)
            continue
        }

        val operation = parseOperation(line)
        if (operation != null) {
            builder?.operation(operation)
            continue
        }

        val test = parseTest(line)
        if (test != null) {
            builder?.test(test)
            continue
        }

        val trueTarget = parseTrueTarget(line)
        if (trueTarget != null) {
            builder?.trueTarget(trueTarget)
            continue
        }

        val falseTarget = parseFalseTarget(line)
        if (falseTarget != null) {
            builder?.falseTarget(falseTarget)
            continue
        }

        throw Exception("Could not parse line: $line")
    }

    if (builder != null) {
        monkeys.put(monkeyNum!!, builder.build())
    }

    return monkeys
}
