package app

import java.io.File
import kotlin.math.abs

val SENSOR_REGEX = """Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)""".toRegex()

data class Point(val x: Int, val y: Int) {
    fun distanceToPoint(other: Point): Int =
        abs(x - other.x) + abs(y - other.y)
}

data class Sensor(val location: Point, val nearestBeacon: Point) {
    private val distance = location.distanceToPoint(nearestBeacon)

    fun containsPoint(point: Point): Boolean =
        location.distanceToPoint(point) <= location.distanceToPoint(nearestBeacon)

    fun xRangeInRow(row: Int): IntRange {
        val leftover = distance - abs(location.y - row)
        val min = location.x - leftover
        val max = location.x + leftover
        return IntRange(min, max)
    }
}

class RangeUnion(private val ranges: Set<IntRange>) {
    val min = ranges.map { Math.min(it.first, it.last) }.minOrNull()!!
    val max = ranges.map { Math.max(it.first, it.last) }.maxOrNull()!!

    fun all(): Set<Int> = ranges.flatMap { it.asSequence() }.toSet()

    fun contains(x: Int): Boolean = ranges.any { it.contains(x) }
}

fun parseLines(lines: List<String>): List<Sensor> =
    lines.map { line ->
        val (locX, locY, beaconX, beaconY) = SENSOR_REGEX.matchEntire(line)!!.destructured
        Sensor(Point(locX.toInt(), locY.toInt()), Point(beaconX.toInt(), beaconY.toInt()))
    }

fun main(args: Array<String>) {
    val y = 2000000
    val sensors = parseLines(File(args.first()).readLines())
    val rangeUnion = RangeUnion(sensors.map { it.xRangeInRow(y) }.toSet())

    val points = rangeUnion.all().map { Point(it, y) }.toSet()
    val actualBeacons = sensors.map { it.nearestBeacon }.toSet()
    val definitelyNoBeacon = points.subtract(actualBeacons)
    println(definitelyNoBeacon.size)
}
