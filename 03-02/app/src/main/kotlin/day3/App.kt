package day3

import java.io.File
import java.util.stream.Collectors

@JvmInline
value class Item(val c: Char) {
    fun getPriority(): Int = when(c) {
        in 'a'..'z' -> c - 'a' + 1
        in 'A'..'Z' -> c - 'A' + 27
        else -> throw Exception("Invalid item code: $c")
    }
}

fun rucksackFromLine(line: String): Set<Item> = line.toCharArray().map { c -> Item(c.toChar()) }.toSet()

fun main(args: Array<String>) {
    val inputFilename = args.firstOrNull()
    if (inputFilename == null) {
        throw Exception("Must provide an input filename")
    }

    var prioritySum = 0
    val rucksacks = mutableListOf<Set<Item>>()

    File(inputFilename).forEachLine() { line ->
        rucksacks.add(rucksackFromLine(line))
        if (rucksacks.size == 3) {
            println(rucksacks)
            val dupe = rucksacks.reduce { intersection, rucksack ->
                intersection.intersect(rucksack)
            }.first()
            println("Dupe was ${dupe.c}")
            prioritySum += dupe.getPriority()
            rucksacks.clear()
        }
    }

    println(prioritySum)
}
