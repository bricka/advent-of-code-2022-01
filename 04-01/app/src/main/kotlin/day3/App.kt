package day3

import java.io.File
import java.util.stream.Collectors

fun IntRange.containsRange(other: IntRange): Boolean = this.start <= other.start && this.endInclusive >= other.endInclusive

fun rangeFromString(str: String): IntRange {
    val (pos1, pos2) = str.split('-')
    return pos1.toInt()..pos2.toInt()
}

fun rangesFromLine(line: String): Pair<IntRange, IntRange> {
    val (range1, range2) = line.split(',')
    return Pair(rangeFromString(range1), rangeFromString(range2))
}

fun main(args: Array<String>) {
    val inputFilename = args.first()
    val overlapping = File(inputFilename).useLines { lines ->
        lines.fold(0) { soFar, line ->
            val (range1, range2) = rangesFromLine(line)
            if (range1.containsRange(range2) || range2.containsRange(range1)) {
                soFar + 1
            } else {
                soFar
            }
        }
    }
    println(overlapping)
}
