package app

import java.io.File
import kotlin.math.sign

class Grid(private val maxX: Int, private val maxY: Int) {
    private val grid = Array(maxX + 1000) { Array(maxY + 3) { false } }

    fun markFull(p: Point) {
        grid[p.x][p.y] = true
    }

    fun isFull(p: Point): Boolean = grid[p.x][p.y] || p.y == maxY + 2

    fun highestFullPointBelow(p: Point): Point {
        for(y in (p.y+1..maxY+1)) {
            if (grid[p.x][y]) {
                return Point(p.x, y)
            }
        }

        return Point(p.x, maxY + 2)
    }
}

data class Point(val x: Int, val y: Int) {
    fun towards(other: Point): Point = if (x == other.x) {
        Point(x, y + (other.y - y).sign)
    } else {
        Point(x + (other.x - x).sign, y)
    }

    fun up(): Point = Point(x, y - 1)
    fun downLeft(): Point = Point(x - 1, y + 1)
    fun downRight(): Point = Point(x + 1, y + 1)

    override fun toString() = "($x, $y)"
}

data class LineSegment(val guidePoints: List<Point>) {
    fun points(): List<Point> {
        var lastPoint = guidePoints.first()
        val points = mutableListOf(lastPoint)

        for (nextPoint in guidePoints.subList(1, guidePoints.size)) {
            while(lastPoint != nextPoint) {
                lastPoint = lastPoint.towards(nextPoint)
                points.add(lastPoint)
            }
        }

        return points
    }

    override fun toString() = guidePoints.joinToString(" -> ")
}

fun parseInput(lines: List<String>): List<LineSegment> {
    val list = mutableListOf<LineSegment>()

    for (line in lines) {
        list.add(LineSegment(line.split(" -> ").map { it.split(",") }.map { Point(it.get(0).toInt(), it.get(1).toInt()) }))
    }

    return list
}

fun dropSandUntilAbyss(grid: Grid): Int {
    var droppedSand = 0

    newsand@ while(true) {
        droppedSand++

        // if (droppedSand > 100) {
        //     return droppedSand - 1
        // }

        var dropPoint = Point(500, 0)
        while (true) {
            // println("Dropping from $dropPoint")
            val whatItHits = grid.highestFullPointBelow(dropPoint)
            val landsAt = whatItHits.up()
            // println("Landed at $landsAt")
            if (grid.isFull(landsAt.downLeft()) && grid.isFull(landsAt.downRight()) && landsAt == Point(500,0)) {
                return droppedSand
            } else if (grid.isFull(landsAt.downLeft()) && grid.isFull(landsAt.downRight())) {
                grid.markFull(landsAt)
                continue@newsand
            }
            if (!grid.isFull(landsAt.downLeft())) {
                dropPoint = landsAt.downLeft()
            } else {
                dropPoint = landsAt.downRight()
            }
        }
    }
}

fun main(args: Array<String>) {
    val lineSegments = parseInput(File(args.first()).readLines())
    val points = lineSegments.flatMap { it.guidePoints }
    val maxX = points.map { it.x }.maxOrNull()!!
    val maxY = points.map { it.y }.maxOrNull()!!

    val grid = Grid(maxX, maxY)
    lineSegments.flatMap { it.points() }.forEach{ it -> grid.markFull(it) }

    println(dropSandUntilAbyss(grid))
}
